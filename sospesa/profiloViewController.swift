//
//  profiloViewController.swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import UIKit

class profiloViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    
    
    var imagePicker = UIImagePickerController()
    var spesa1 = ViewController()
    
    
    var spesa = cloudController()
    @IBOutlet weak var negozioImmagine: UIImageView!
    
    
    @IBOutlet weak var negozioVia: UITextField!
    @IBOutlet weak var negozioNome: UITextField!

    @IBOutlet weak var negozioOrario: UITextField!
    
   
    @IBOutlet weak var negozioNumeroBuste1: UITextField!
    
    @IBOutlet weak var negozioImmagineBusta: UIImageView!
    
    
    @IBOutlet weak var negozioNumeroBuste: UILabel!
    
    @IBOutlet weak var invioDatabase: UIButton!
    
    @IBAction func invioDatabaseAction(_ sender: Any) {
        self.spesa.salvaNota(nomeNegozio: negozioNome.text!, viaNegoizo: negozioVia.text!, orarioApertura: negozioOrario.text!, numeroBuste: negozioNumeroBuste1.text!, immagineNegozio: negozioImmagine.image! )
        
       
    }
    
    
    
//    funzioni apertura camera scelta foto
    
    @IBAction func AddPhoto(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                          self.openCamera()
                     }))

                      alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                          self.openGallary()
                      }))

                      alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

                      /*If you want work actionsheet on ipad
                      then you have to use popoverPresentationController to present the actionsheet,
                      otherwise app will crash on iPad */
                      switch UIDevice.current.userInterfaceIdiom {
                      case .pad:
                        alert.popoverPresentationController?.sourceView = sender as? UIView
                        alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                          alert.popoverPresentationController?.permittedArrowDirections = .up
                      default:
                          break
                      }

                      self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
               {
                  if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
                   {
                      imagePicker.sourceType = UIImagePickerController.SourceType.camera
                       imagePicker.allowsEditing = true
                       self.present(imagePicker, animated: true, completion: nil)
                   }
                   else
                   {
                       let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                   }
               }
              func openGallary()
              {
                 imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                  imagePicker.allowsEditing = true
                  self.present(imagePicker, animated: true, completion: nil)

              }
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
               negozioImmagine.contentMode = .scaleAspectFit
               negozioImmagine.image = pickedImage
           }

           dismiss(animated: true, completion: nil)
       }

    
    override func viewDidLoad() {
        super.viewDidLoad()
       imagePicker.delegate = self
                // Do any additional setup after loading the view.
    }
}
