//
//  cloudController.swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import Foundation
import CloudKit
import UIKit

protocol cloudControllerDelegate {
    func errorUpdating(error: NSError)
    func modelUpdated()
}
class cloudController {
    var container: CKContainer
    var publicDB: CKDatabase
    let privateDB: CKDatabase
    var negozioArray = [Negozio]() // conterrà tutti i Record recuperati dal Container.
    var delegate : cloudControllerDelegate?
    
    
    
    
    init() {
        container = CKContainer.default()
        publicDB = container.publicCloudDatabase
        privateDB = container.privateCloudDatabase
    }
    func salvaNota(nomeNegozio: String, viaNegoizo: String, orarioApertura: String, numeroBuste: String, immagineNegozio: UIImage ) {
        
        let recordNegozio = CKRecord(recordType: "Negozio")
        
       recordNegozio.setValue(nomeNegozio, forKey: "nomeNegozio")
        recordNegozio.setValue(viaNegoizo, forKey: "viaNegozio")
        recordNegozio.setValue(orarioApertura, forKey: "orarioApertura")
        recordNegozio.setValue(numeroBuste, forKey: "numeroBuste")
        
        guard let imageURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("immagineNegozio") else {
            return }
       do { try immagineNegozio.pngData()?.write(to: imageURL)
            try immagineNegozio.jpegData(compressionQuality: 0)?.write(to: imageURL)
        }
        catch {print("c'è un errore")}
        
        let imageAssets = CKAsset(fileURL: imageURL)
       
//        imageAssets.setValue(immagineNegozio, forKey: "immagineNegozio")
        recordNegozio["immagineNegozio"] = imageAssets
        privateDB.save(recordNegozio)  { (record, error) -> Void in
            _ = imageAssets
            _ = recordNegozio
            
            print("nota salvata con successo")
                
           
    }
    }

    func recuperaNote() {
        /*
            Il predicate è il modo in cui i dati devono essere recuperati (ad esempio solo quelli che hanno un certo valore)
            // La CKQuery è il tipo di domanda da fare al database. In particolare si sta richiedendo tutti i tipi record di tipo "Nota" con nessun tipo di filtro (dato che predicate non è impostato)
        */
       
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Negozio", predicate: predicate)
        
       
        let imageURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("immagineNegozio")
        
        
        
        // performQuery manda in esecuzione la query. il metodo è una closure che restituisce results (un array di anyObject) o error in caso di query fallita
        privateDB.perform(query, inZoneWith: nil) { (results, error) -> Void in
            // se l'errore è diverso da nil quindi è stato generato un errore
            if error != nil {
                // vedi l'articolo sul blog per capire cosa succede qui dentro
                DispatchQueue.main.async {
                 self.delegate?.errorUpdating(error: error! as NSError)
                    
                    print("negozio non caricata con successo")
                     return
                }
//                 dispatch_queue_main_t().async()  {
//                    self.delegate?.errorUpdating(error: error! as NSError)
                

                  
//                }
            } else {
                self.negozioArray.removeAll(keepingCapacity: true)
                // se non è stato generato un errore allora itera tutti gli oggetti dell'array result
               
                    /*
                        crea una nota trasformando il record in CKRecord dato che è un AnyObject
                        e inserisce la nota all'interno dell'array di note
                 
                    */
               
              for recordNegozio in results! {
               
               
               
               let imageAssets = CKAsset(fileURL: imageURL!)
                
                                 
                let negozio = Negozio(record: recordNegozio as CKRecord, database: self.privateDB, asset: imageAssets)
                
           
                self.negozioArray.append(negozio)
                print("negozio caricata con successo")
                
                
                    }
                DispatchQueue.main.async {
                           self.delegate?.modelUpdated()
                                           return
                           }
            // vedi l'articolo sul blog per capire cosa succede
//            dispatch_queue_main_t().async() {
//                self.delegate?.modelUpdated()
//                return
//            }
       }
    
}
}
}

