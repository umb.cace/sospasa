//
//  SOSpesa(record).swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import Foundation
import CloudKit
import UIKit

class Negozio: NSObject {
    var record: CKRecord!
    var asset: CKAsset!
    var nomeNegozio: String!
    var ViaNegozio: String!
    var immagineNegozio: UIImage!
    var numeroBuste: String!
    var orarioApertura: String!
   
    
    
    var database: CKDatabase!
    var date: NSDate
    
    init(record : CKRecord, database: CKDatabase, asset : CKAsset) {
        self.record = record
        self.asset = asset
        self.database = database
        self.nomeNegozio = record.object(forKey: "nomeNegozio") as? String
        self.immagineNegozio = record.object(forKey: "immagineNegozio") as? UIImage
        self.ViaNegozio = record.object(forKey: "viaNegozio") as? String
        self.orarioApertura = record.object(forKey: "orarioApertura") as? String
        self.numeroBuste = record.object(forKey: "numeroBuste") as? String
      
         self.date = record.creationDate! as NSDate
    }
    
    
}
