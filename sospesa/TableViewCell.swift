//
//  TableViewCell.swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var immagineNegozio: UIImageView! 
    @IBOutlet weak var nomeNegozio: UILabel!
    
    @IBOutlet weak var viaNegozio: UILabel!
    
    @IBOutlet weak var orarioApertura: UILabel!
    @IBOutlet weak var orario: UILabel!
    
    @IBOutlet weak var busta: UIImageView!
    @IBOutlet weak var numeroBuste: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
