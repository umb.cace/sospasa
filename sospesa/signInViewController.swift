//
//  signInViewController.swift
//  sospesa
//
//  Created by umberto cacace on 05/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
import UIKit
import GoogleSignIn

@objc(signInViewController)
class signInViewController: UIViewController {
   
//    @IBOutlet weak var signInButton: GIDSignInButton!
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var statusText: UILabel!
   
    override func viewDidLoad() {
        GIDSignIn.sharedInstance()?.presentingViewController = self

        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()

        // [START_EXCLUDE]
        NotificationCenter.default.addObserver(self,
            selector: #selector(signInViewController.receiveToggleAuthUINotification(_:)),
            name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil)

        statusText.text = "Initialized Swift app..."
        toggleAuthUI()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
       GIDSignIn.sharedInstance().signOut()
       // [START_EXCLUDE silent]
       statusText.text = "Signed out."
       toggleAuthUI()
       // [END_EXCLUDE]
     }
     // [END signout_tapped]
     // [START disconnect_tapped]
     @IBAction func didTapDisconnect(_ sender: AnyObject) {
       GIDSignIn.sharedInstance().disconnect()
       // [START_EXCLUDE silent]
       statusText.text = "Disconnecting."
       // [END_EXCLUDE]
     }
     // [END disconnect_tapped]
     // [START toggle_auth]
     func toggleAuthUI() {
       if let _ = GIDSignIn.sharedInstance()?.currentUser?.authentication {
         // Signed in
         signInButton.isHidden = true
         signOutButton.isHidden = false
         disconnectButton.isHidden = false
       } else {
         signInButton.isHidden = false
         signOutButton.isHidden = true
         disconnectButton.isHidden = true
         statusText.text = "Google Sign in\niOS Demo"
       }
     }
     // [END toggle_auth]
     override var preferredStatusBarStyle: UIStatusBarStyle {
       return UIStatusBarStyle.lightContent
     }

     deinit {
       NotificationCenter.default.removeObserver(self,
           name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
           object: nil)
     }

     @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
       if notification.name.rawValue == "ToggleAuthUINotification" {
         self.toggleAuthUI()
         if notification.userInfo != nil {
           guard let userInfo = notification.userInfo as? [String:String] else { return }
           self.statusText.text = userInfo["statusText"]!
         }
       }
     }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
