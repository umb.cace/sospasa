//
//  sospesa.swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import Foundation
import UIKit
class negozio {
    var immagineDelNegozio: UIImage?
    var immagineBusta: UIImage?
    var nomeDelNegozio: String?
    var viaDelNegozio: String?
    var orarioAperturaNegozio: String?
    var orarioEffettivo: String?
    var numeroBustePresenti: String?
    
    init(immagineDelNegozio: UIImage, immagineBusta: UIImage,nomeDelNegozio: String,  viaDelNegozio: String, orarioAperturaNegozio: String, orarioEffettivo: String,numeroBustePresenti: String ) {
        self.immagineDelNegozio = immagineDelNegozio
        self.immagineBusta = immagineBusta
        self.nomeDelNegozio = nomeDelNegozio
        self.viaDelNegozio = viaDelNegozio
        self.orarioAperturaNegozio = orarioAperturaNegozio
        self.orarioEffettivo = orarioEffettivo
        self.numeroBustePresenti = numeroBustePresenti
    }
    
}
