//
//  ViewController.swift
//  sospesa
//
//  Created by umberto cacace on 04/04/2020.
//  Copyright © 2020 umberto cacace. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, cloudControllerDelegate {
    
    
    var a = cloudController()
    @IBOutlet weak var ArriornaTabella: UIButton!
    
    @IBAction func AggiornaTabella1(_ sender: Any) {
        spesa.recuperaNote()
    }
    func errorUpdating(error: NSError) {
        let alertController = UIAlertController(
                title: "Errore CloudKit",
                message: error.localizedDescription,
                preferredStyle: UIAlertController.Style.alert
            )
            
        let exit = UIAlertAction(title: "Chiudi", style: UIAlertAction.Style.cancel,
                handler: {(paramAction:UIAlertAction!) in
                    print("[AlertView] Chiusa")
            })
            alertController.addAction(exit)
            
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func modelUpdated() {
        return tableView.reloadData()
    }
    var spesa = cloudController()
 func numberOfSectionsInTableView(tableView: UITableView) -> Int {
          return 1
      }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spesa.negozioArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let spesanecessaria = spesa.negozioArray[indexPath.row]
        let cell: TableViewCell = tableView.dequeueReusableCell(withIdentifier: "homepage", for: indexPath) as! TableViewCell
        cell.immagineNegozio.image = spesanecessaria.immagineNegozio
        cell.nomeNegozio?.text = spesanecessaria.nomeNegozio
        cell.viaNegozio?.text = spesanecessaria.ViaNegozio
        cell.orarioApertura?.text = spesanecessaria.orarioApertura
        cell.numeroBuste?.text = spesanecessaria.numeroBuste
        
        return cell
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        spesa.delegate = self
       
        // Do any additional setup after loading the view.
    }


}

